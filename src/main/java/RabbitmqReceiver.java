import com.rabbitmq.client.*;
import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData.Record;
//import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;

import javax.print.DocFlavor;
import java.io.IOException;
import java.util.concurrent.TimeoutException;


public class RabbitmqReceiver implements Receiver{
    private Channel channel;
    private String queue_name;
    //final static Logger logger = LoggerFactory.getLogger(RabbitmqReceiver.class);
    //static Logger logger = Logger.getLogger(RabbitmqReceiver.class);
    static String redisSaverHost = System.getenv("REDIS_SAVER_HOST");

    //private BufferedWriter writer = null;


    /*
    public RabbitmqReceiver(Map<String,Schema> eventsSchema) {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        factory.setUsername("cep-handler");
        factory.setPassword("g7wavG!WZ&5cYXu@yeR6nnho97A7Sd");

        try {
            Connection connection = factory.newConnection();
            channel = connection.createChannel();
            String exchange_name = "EXCHANGE";
            channel.exchangeDeclare(exchange_name, "topic");
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, exchange_name, "#");
            //channel.queueBind(queue_name, exchange_name, "generated.#");
            //writer = new BufferedWriter(new FileWriter(new File("data/messages"), true));
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
        System.out.println("Defining consumer");

        //j = new Jedis("redis-saver-svc");
        //System.out.print(" - Event Received: "+event.getSchema().getName());
        //long deliveryTag = envelope.getDeliveryTag();
        //channel.basicAck(deliveryTag, false);

        //try {
        //    writer = new BufferedWriter(new FileWriter(new File("data/mensagens.txt")));
        //} catch (IOException e) {
        //    e.printStackTrace();
        //}


        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){
                //scheme = eventsSchema.get(envelope.getRoutingKey());
                //System.out.print(" . ");
                //System.out.println(AvroDeserialize(body,scheme));
                //FileWriter writer = new FileWriter(new File("data/messages" + scheme.getName()), true);
                //eventsFile.get(envelope.getRoutingKey()).append(AvroDeserialize(body, eventsSchema.get(envelope.getRoutingKey())).toString()).append(" , ").append(String.valueOf(System.currentTimeMillis())).flush();
                //new BufferedWriter(new FileWriter(new File("data/mensage"+scheme.getName()))).append(scheme.getName()).append(", ").append(AvroDeserialize(body,scheme).toString()).append(" , ").append(String.valueOf(System.currentTimeMillis())).flush();
                //j.sadd(scheme.getName(),AvroDeserialize(body,scheme)+" , "+System.currentTimeMillis());

                //writer.close();
            }
        };
        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


     */
    public RabbitmqReceiver(ConnectionFactory factory, String eventId, Schema schema) {
        final Schema scheme = schema;
        Jedis jay = new Jedis(redisSaverHost,6377);
        jay.auth("#5ip3Je3uyS7QxUyDf@2Ajj7c#V7tXk");
        //BasicConfigurator.configure();
        //PropertyConfigurator.configure("log4j.properties");

        try {
            //Connection connection = factory.newConnection();
            Connection conn = factory.newConnection();
            channel = conn.createChannel();
            String exchange_name = "EXCHANGE";
            channel.exchangeDeclare(exchange_name, "topic");
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, exchange_name, eventId);
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){
                long timestamp = System.currentTimeMillis();
                String evt = AvroDeserialize(body,scheme).toString();

                //System.out.println(evt);

                //SendToLog(schema.getName()+" ; "+evt+" ; "+timestamp);
                jay.hset(schema.getName(),String.valueOf(timestamp),evt);


                /*
                try {
                    new BufferedWriter(new FileWriter(new File("data/mensage"+scheme.getName()))).append(evt).append(" , ").append(String.valueOf(timestamp)).flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */

            }
        };
        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void CloseConnection()  {
        try {
            channel.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }


    public RabbitmqReceiver(Connection conn, String eventId, Schema schema,Jedis j) {
        final Schema scheme = schema;
        Jedis jay =j;

        try {
            //Connection connection = factory.newConnection();
            channel = conn.createChannel();
            String exchange_name = "EXCHANGE";
            channel.exchangeDeclare(exchange_name, "topic");
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, exchange_name, eventId);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){
                long timestamp = System.currentTimeMillis();
                String evt = AvroDeserialize(body,scheme).toString();

                //System.out.println(evt);

                //SendToLog(schema.getName()+" ; "+evt+" ; "+timestamp);
                jay.hset(schema.getName(),String.valueOf(timestamp),evt);
            }
        };
        try {
            channel.basicConsume(queue_name, true, "myConsumerTag", consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public RabbitmqReceiver(Channel channel, String eventId, Schema schema, Jedis j) {
        final Schema scheme = schema;
        //System.out.println("create receiver for : "+schema.getName());

        Jedis jay =j;

        try {
            queue_name = channel.queueDeclare().getQueue();
            channel.queueBind(queue_name, "EXCHANGE", eventId);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body){
                long timestamp = System.currentTimeMillis();
                String evt = AvroDeserialize(body,scheme).toString();

                //System.out.println();
                //System.out.println("Schema name : "+schema.getName());
                //SendToLog(schema.getName()+" ; "+evt+" ; "+timestamp);
                jay.hset(schema.getName(),String.valueOf(timestamp),evt);
            }
        };
        try {
            channel.basicConsume(queue_name, true, schema.getName()+eventId, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    private static Record AvroDeserialize(byte [] bytes,Schema schema){
        Injection<Record, byte[]> recordInjection;
        recordInjection = GenericAvroCodecs.toBinary(schema);
        return recordInjection.invert(bytes).get();
    }

    /*public static void SendToLog(String tolog){
        logger.info(tolog);
        //logger.trace(tolog);
        //logger.debug(tolog);
    }

     */

}
