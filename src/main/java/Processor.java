import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
//import org.apache.log4j.Logger;
//import org.apache.log4j.PropertyConfigurator;
import redis.clients.jedis.Jedis;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;

import static java.lang.System.exit;

public class Processor {

    //private static final String Broker = System.getenv("BROKER");
    static String redisSaverHost = System.getenv("REDIS_SAVER_HOST");

    static boolean add_latency = Boolean.parseBoolean(System.getenv("ADDED_LATENCY"));
    static String nameFile = System.getenv("NAME_FILE");
    //private static final String types = "savethis";
    //System.getenv("TYPES");
    //private static final String checkredis = System.getenv("CHECK_REDIS");

    private static final int td = Integer.parseInt(System.getenv("TD"));
    private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    //static Logger logger = Logger.getLogger(Processor.class);
    //Integer.parseInt(System.getenv("TD"));

    static ZoneId SaoPauloTimezone = ZoneId.of("America/Sao_Paulo");

    public static void main(String[] args) {
        ZonedDateTime nowSaoPaulo = ZonedDateTime.ofInstant(Instant.now(), SaoPauloTimezone);
        //LocalDateTime today = LocalDateTime.now();
        System.out.println("Starting Experiment at "+nowSaoPaulo.format(dtF));

        Jedis j;
        j = new Jedis(System.getenv("REDIS_HOST"));
        j.auth("2KXcBK8#gUq3wAgsf77LheGkjWv@HhZE");
        j.set("Worker1InitTime", String.valueOf(System.currentTimeMillis()));

        Set<String> threads = j.smembers("savethis");
        Map<String, Schema> typesSchema = new ConcurrentHashMap<>();
        if(System.getenv("CHECK_REDIS").equals("true")) {
            boolean startSendingEvents = false;
            if (j.zcard("Unnasigned") == null) {
                System.out.print("Unnasigned null");
                exit(1);
            }
            boolean jz = j.zcard("Unnasigned").equals(0L);
            if (jz) startSendingEvents = true;
            while (!startSendingEvents) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (j.zcard("Unnasigned") == null) {
                    System.out.print("Unnasigned null");
                    exit(1);
                }
                jz = j.zcard("Unnasigned").equals(0L) || j.zcard("Unnasigned") == null;
                if (jz) startSendingEvents = true;
            }
            System.out.println("Startin registering ");

            if (j.scard("savethis") == null) {
                System.out.print("types null");
                exit(1);
            }

            for (String eventType : threads) {
                if (j.get(eventType + ":AvroSchema") == null) {
                    System.out.print("types null for eventType "+eventType);
                    exit(1);
                }
                String schemeString = j.get(eventType + ":AvroSchema");
                //System.out.println("type: "+eventType+" - schema: "+schemeString);
                Schema schema = new Schema.Parser().parse(schemeString);
                typesSchema.put(eventType, schema);
            }
            System.out.println("Ending Registering");

        }

        System.out.println("Saving schemas");
        if(typesSchema.isEmpty()){
            Set<String> basicevents = j.smembers("savethis");
            for(String be : basicevents){
                typesSchema.put(be,new Schema.Parser().parse(j.get(be+":AvroSchema")));
            }
        }
        System.out.println("Startin Rabbit Client");

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        factory.setUsername("cep-handler");
        factory.setPassword("g7wavG!WZ&5cYXu@yeR6nnho97A7Sd");
        int k =0;

        Map<Integer,Connection> mapcon = new ConcurrentHashMap<>();
        Map<Integer, Channel> mapchan = new ConcurrentHashMap<>();
        Map<Integer,Jedis> mapjedis = new ConcurrentHashMap<>();
        Map<String, RabbitmqReceiver> receivers = new ConcurrentHashMap<>();
        int i =0;
        Connection connection = null;
        Channel channel = null;
        Jedis jay = null;
        System.out.println("Creating Receivers");

        for(String key : typesSchema.keySet()){
            if(i==2000) i =0;
            if(i==0) {
                jay = new Jedis(redisSaverHost, 6377);
                jay.auth("#5ip3Je3uyS7QxUyDf@2Ajj7c#V7tXk");
                try {
                    connection = factory.newConnection();
                    channel = connection.createChannel();
                    channel.exchangeDeclare("EXCHANGE", "topic");
                } catch (IOException | TimeoutException e) {
                    e.printStackTrace();
                }
                mapcon.put(k,connection);
                mapjedis.put(k,jay);
                k++;
            }
            receivers.put(key,new RabbitmqReceiver(channel,key,new Schema.Parser().parse(j.get(key+":AvroSchema")),jay));
            i++;
        }
        System.out.println("Receivers Created");



        Sender sender = new RabbitmqSender();


        //if(System.getenv("EXPERIMENTAL").equals("true")){
        System.out.println("Mode : EXPERIMENTAL");
        experimental(sender);
        //}
        //else if(System.getenv("CARE").equals("false")){ full_power(sender); }
        //else original(sender);

        sender.CloseConnection();
        Instant nowUtc2 = Instant.now();

        ZonedDateTime nowSaoPaulo2 = ZonedDateTime.ofInstant(nowUtc2, SaoPauloTimezone);
        //LocalDateTime today = LocalDateTime.now();
        j.set("EndExperiment", String.valueOf(System.currentTimeMillis()));
        j.close();
        System.out.println("\nTerminado at "+nowSaoPaulo2.format(dtF));
    }


    private static String ASchema(String TypeName){
        return "{" +
                "  \"type\" : \"record\"," +
                "  \"name\" : \""+TypeName+"\"," +
                "  \"fields\" : [ " +
                "{ \"name\" : \"c\",   \"type\" : { \"type\" : \"string\", \"avro.java.string\" : \"String\" } }," +
                "{ \"name\" : \"cl\",  \"type\" : \"int\" }," +
                "{ \"name\" : \"sl\",  \"type\" : \"int\" }," +
                "{ \"name\" : \"qv\",  \"type\" : \"int\" }," +
                "{ \"name\" : \"p\",   \"type\" : \"int\" }," +
                "{ \"name\" : \"a\",   \"type\" : \"boolean\" }," +
                "{ \"name\" : \"ta\",  \"type\" : { \"type\" : \"long\", \"logicalType\" : \"timestamp-millis\" } }," +
                "{ \"name\" : \"py\",  \"type\" : \"double\" }," +
                "{ \"name\" : \"px\",  \"type\" : \"double\" }," +
                "{ \"name\" : \"timestamp\",  \"type\" : { \"type\" : \"long\", \"logicalType\" : \"timestamp-millis\" } }" +
                "] }";
    }

    private static Schema generateBusSchema(String TypeName) {
        Schema.Parser parser = new Schema.Parser();
        return parser.parse(ASchema(TypeName));
    }

    private static byte[] AvroSerialize(GenericData.Record Event){
        Injection<GenericData.Record, byte[]> recordInjection;
        recordInjection = GenericAvroCodecs.toBinary(Event.getSchema());
        return recordInjection.apply(Event);
    }

    static DateTimeFormatter dtF  = DateTimeFormatter.ofPattern("kk:mm:ss  dd/MM/yyyy");

    private static void Send_Thread_Events(SortedMap<Long,Set<Integer>> timetable,ConcurrentHashMap<Integer,SortedMap<Long,byte []>> serial_events, Sender s, Map<Integer,Schema> map_schemas){
        //x.submit(() -> {
        Long current_ta = timetable.firstKey();
        Set<Integer> current_cl = timetable.get(current_ta);
        Long next_ta;
        Set<Integer> next_cl;
        timetable.remove(current_ta);
        GenericData.Record busEvent;
        System.out.println("***************************************************************************");
        System.out.println("***************************************************************************");
        System.out.println("***************************************************************************");
        //Date date = new Date();
        Instant nowUtc = Instant.now();

        ZonedDateTime nowSaoPaulo = ZonedDateTime.ofInstant(nowUtc, SaoPauloTimezone);
        //LocalDateTime today = LocalDateTime.now();
        System.out.println("X | Sending Events. Starting at "+nowSaoPaulo.format(dtF));
        System.out.println("***************************************************************************");
        System.out.println("***************************************************************************");
        System.out.println("***************************************************************************");
        Jedis j = new Jedis(System.getenv("REDIS_HOST"));
        j.auth(System.getenv("REDIS_PASS"));
        j.set("StartExpTimestamp",String.valueOf(System.currentTimeMillis()));
        j.close();
        while(!timetable.isEmpty()){
            next_ta = timetable.firstKey();
            next_cl = timetable.get(next_ta);
            timetable.remove(next_ta);
            for(Integer cls: current_cl){
                busEvent = AvroDeserialize(serial_events.get(cls).get(current_ta),map_schemas.get(cls));
                busEvent.put("timestamp",System.currentTimeMillis());
                s.send(String.valueOf(cls),AvroSerialize(busEvent));

            }
            current_cl = next_cl;
            try {
                TimeUnit.MILLISECONDS.sleep(next_ta-current_ta);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            current_ta = next_ta;

        }
        //});
    }

    /*
    private static void Send_Max_Events(SortedMap<Long,Set<Integer>> timetable,ConcurrentHashMap<Integer,SortedMap<Long,byte []>> serial_events, Sender s, Map<Integer,Schema> map_schemas){
        long current_ta;
        Set<Integer> current_cl;
        GenericData.Record busEvent;
        System.out.println("X | Sending max Events ");
        while(!timetable.isEmpty()){
            current_ta = timetable.firstKey();
            current_cl = timetable.get(current_ta);
            timetable.remove(current_ta);
            for(Integer cls: current_cl){
                busEvent = AvroDeserialize(serial_events.get(cls).get(current_ta),map_schemas.get(cls));
                busEvent.put("timestamp",System.currentTimeMillis());
                s.send(String.valueOf(cls),AvroSerialize(busEvent));
            }
        }
    }


     */
    public static GenericData.Record AvroDeserialize(byte [] bytes, Schema schema){
        Injection<GenericData.Record, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);
        return recordInjection.invert(bytes).get();
        //return ((Injection<GenericData.Record,byte[]>)GenericAvroCodecs.toBinary(schema)).invert(bytes).get();
    }

    private static void experimental(Sender sender){
        BufferedReader reader = null;
        //System.out.println("Open buffer");
        String line = null;
        String[] clevent;
        long ta;
        int cl;
        int sl;
        int p;
        int qv;
        boolean a;
        double py;
        double px;
        Schema schema;
        long e_ts;
        GenericData.Record busEvent;
        ConcurrentHashMap<Integer,SortedMap<Long,byte []>> serial_events = new ConcurrentHashMap<>();
        SortedMap<Long,Set<Integer>> timetable = new TreeMap<>();
        ConcurrentHashMap<Integer,Schema> mapa_schemas = new ConcurrentHashMap<>();
        try {
            reader = new BufferedReader(new FileReader(new File(nameFile)));
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("XE | read file");
        while (line != null) {
            clevent = line.split("\\s*,\\s*");
            cl = Integer.parseInt(clevent[0]);
            sl = Integer.parseInt(clevent[2]);
            p = Integer.parseInt(clevent[3]);
            qv = Integer.parseInt(clevent[4]);
            a = Boolean.parseBoolean(clevent[5]);
            ta = Long.parseLong(clevent[6]);
            if(ta>1572864648000L) {
                py = Double.parseDouble(clevent[7]);
                px = Double.parseDouble(clevent[8]);
                if (!mapa_schemas.containsKey(cl)) {
                    schema = generateBusSchema("bus" + clevent[0]);
                    mapa_schemas.put(cl, schema);
                } else {
                    schema = mapa_schemas.get(cl);
                }
                busEvent = new GenericData.Record(schema);
                busEvent.put("cl", cl);
                busEvent.put("c", clevent[1]);
                busEvent.put("sl", sl);
                busEvent.put("p", p);
                busEvent.put("qv", qv);
                busEvent.put("a", a);
                busEvent.put("ta", ta);
                busEvent.put("py", py);
                busEvent.put("px", px);
                busEvent.put("timestamp", 0L);
                if (!serial_events.containsKey(cl)) {
                    serial_events.put(cl, new TreeMap<>());
                }
                serial_events.get(cl).put(ta, AvroSerialize(busEvent));
                if (!timetable.containsKey(ta)) {
                    timetable.put(ta, new HashSet<>());
                    //System.out.println(ta);
                }
                timetable.get(ta).add(cl);
            }
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("XE | file all load up");
        //ExecutorService X = null;
        Send_Thread_Events(timetable,serial_events,sender,mapa_schemas);
        //if(add_latency)
        //else Send_Max_Events(timetable,serial_events,sender,mapa_schemas);


    }

    /*
    private static void original(Sender sender){
        Jedis j;
        j = new Jedis(System.getenv("REDIS_HOST"));
        j.auth("2KXcBK8#gUq3wAgsf77LheGkjWv@HhZE");
        Map<Integer,GenericData.Record> mapadeevents = new HashMap<>();
        Set<String> basicEvtType = j.smembers("basic");
        for(String BET : basicEvtType){
            mapadeevents.put(Integer.valueOf(BET),new GenericData.Record(generateBusSchema("bus"+BET)));
        }
        j.close();

        long ctm = System.currentTimeMillis();
        BufferedReader reader = null;
        //System.out.println("Open buffer");
        String line = null;
        String[] clevent;
        long ta;
        int cl;
        int sl;
        int p;
        int qv;
        boolean a;
        double py;
        double px;
        Schema schema;
        long e_ts;
        GenericData.Record busEvent;
        try {
            reader = new BufferedReader(new FileReader(new File(nameFile)));
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //System.out.println("read line: "+line);
        String times = "0";
        if (line != null) {
            times = line.split("\\s*,\\s*")[6];
        }
        if(times.equals("0"))
            exit(1);
        long tl = Long.parseLong(times);
        long diffe = ctm - tl;
        //System.out.print("diff :" + diffe);

        if (add_latency) {
            while (line != null) {
                //System.out.print("enter loop while");
                clevent = line.split("\\s*,\\s*");
                cl = Integer.parseInt(clevent[0]);
                sl = Integer.parseInt(clevent[2]);
                p = Integer.parseInt(clevent[3]);
                qv = Integer.parseInt(clevent[4]);
                a = Boolean.parseBoolean(clevent[5]);
                ta = Long.parseLong(clevent[6]);
                py = Double.parseDouble(clevent[7]);
                px = Double.parseDouble(clevent[8]);
                //schema = generateBusSchema("bus" + clevent[0]);
                //busEvent = new GenericData.Record(schema);
                busEvent = mapadeevents.get(cl);
                if (busEvent == null) {
                    mapadeevents.put(cl, new GenericData.Record(generateBusSchema("bus" + cl)));
                    busEvent = mapadeevents.get(cl);
                    System.out.println("Missing bus evet" + cl);
                }
                busEvent.put("cl", cl);
                busEvent.put("c", clevent[1]);
                busEvent.put("sl", sl);
                busEvent.put("p", p);
                busEvent.put("qv", qv);
                busEvent.put("a", a);
                busEvent.put("ta", ta);
                busEvent.put("py", py);
                busEvent.put("px", px);

                e_ts = System.currentTimeMillis() - diffe;

                if (ta > e_ts) {
                    //System.out.print("entered loop wait\n");
                    //System.out.print(ta+"\n");
                    try {
                        TimeUnit.SECONDS.sleep(td);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                busEvent.put("timestamp", System.currentTimeMillis());
                //System.out.print("Event sent: "+busEvent.toString()+"\n");

                // read next line
                sender.send(String.valueOf(cl), AvroSerialize(busEvent));

                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        } else {
            //System.out.println("laço correto");
            while (line != null) {
                //System.out.print("enter loop while");
                clevent = line.split("\\s*,\\s*");

                cl = Integer.parseInt(clevent[0]);
                sl = Integer.parseInt(clevent[2]);
                p = Integer.parseInt(clevent[3]);
                qv = Integer.parseInt(clevent[4]);
                a = Boolean.parseBoolean(clevent[5]);
                ta = Long.parseLong(clevent[6]);
                py = Double.parseDouble(clevent[7]);
                px = Double.parseDouble(clevent[8]);

                //Map<String,GenericData.Record> mapadeevents = new HashMap<>();

                //schema = generateBusSchema("bus" + clevent[0]);
                //busEvent = new GenericData.Record(schema);
                busEvent = mapadeevents.get(cl);
                if (busEvent == null) {
                    mapadeevents.put(cl, new GenericData.Record(generateBusSchema("bus" + cl)));
                    busEvent = mapadeevents.get(cl);
                    System.out.println("Missing bus evet" + cl);
                }

                busEvent.put("cl", cl);
                busEvent.put("c", clevent[1]);
                busEvent.put("sl", sl);
                busEvent.put("p", p);
                busEvent.put("qv", qv);
                busEvent.put("a", a);
                busEvent.put("ta", ta);
                busEvent.put("py", py);
                busEvent.put("px", px);
                busEvent.put("timestamp", System.currentTimeMillis());
                sender.send(String.valueOf(cl), AvroSerialize(busEvent));
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }


        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void full_power(Sender sender){
        BufferedReader reader = null;
        //System.out.println("Open buffer");
        String line = null;
        String[] clevent;
        long ta;
        int cl;
        int sl;
        int p;
        int qv;
        boolean a;
        double py;
        double px;
        Schema schema;
        GenericData.Record busEvent;
        SortedMap<Long,Set<GenericData.Record>> timetable = new TreeMap<>();
        ConcurrentHashMap<Integer,Schema> mapa_schemas = new ConcurrentHashMap<>();
        try {
            reader = new BufferedReader(new FileReader(new File(nameFile)));
            line = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("X | read file");
        while (line != null) {
            clevent = line.split("\\s*,\\s*");
            cl = Integer.parseInt(clevent[0]);
            sl = Integer.parseInt(clevent[2]);
            p = Integer.parseInt(clevent[3]);
            qv = Integer.parseInt(clevent[4]);
            a = Boolean.parseBoolean(clevent[5]);
            ta = Long.parseLong(clevent[6]);
            if(ta>1572864648000L) {
                py = Double.parseDouble(clevent[7]);
                px = Double.parseDouble(clevent[8]);
                if (!mapa_schemas.containsKey(cl)) {
                    schema = generateBusSchema("bus" + clevent[0]);
                    mapa_schemas.put(cl, schema);
                } else {
                    schema = mapa_schemas.get(cl);
                }
                busEvent = new GenericData.Record(schema);
                busEvent.put("cl", cl);
                busEvent.put("c", clevent[1]);
                busEvent.put("sl", sl);
                busEvent.put("p", p);
                busEvent.put("qv", qv);
                busEvent.put("a", a);
                busEvent.put("ta", ta);
                busEvent.put("py", py);
                busEvent.put("px", px);
                busEvent.put("timestamp", 0L);

                if (!timetable.containsKey(ta)) {
                    timetable.put(ta, new HashSet<>());
                    //System.out.println(ta);
                }
                timetable.get(ta).add(busEvent);
            }
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("X | file all load up");
        Set<GenericData.Record> current_cl;
        System.out.println("X | Sending Full max Events ");
        while(!timetable.isEmpty()){
            ta = timetable.firstKey();
            current_cl = timetable.get(ta);
            timetable.remove(ta);
            for(GenericData.Record cls: current_cl){
                cls.put("timestamp",System.currentTimeMillis());
                sender.send(String.valueOf(cls),AvroSerialize(cls));
            }
        }
    }

     */
}
