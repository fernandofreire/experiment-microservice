import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import io.nats.client.*;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;


public class NATSReceiver implements Receiver {

    private Connection nc;
    private Dispatcher d;
    private String TypeName;
    //private String subject;
    private Schema schema;

    private Subscription sub;

    public NATSReceiver(Map<String,Schema> eventsSchem){
        String NatsHost = System.getenv("NATS_HOST");
        String NatsPort = System.getenv("NATS_PORT");
        try {
            nc = Nats.connect("nats://"+NatsHost+":"+NatsPort);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        //subject = bindingKey;
        this.schema = schema;

        this.TypeName = TypeName;
        d = nc.createDispatcher((msg) -> {
            GenericData.Record event = AvroDeserialize(msg.getData(), schema);

            //System.out.print("Evento chegou : \n"+schema+"\n"+event.getSchema().toString()+"\n"+event.toString()+"\n");
            System.out.println(event.toString());
            Long tm = System.currentTimeMillis();

            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter("messages"+msg.getSubject(), true));
                writer.append("\n").append(event.toString()).append(" , ").append(tm.toString());
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }


        });
        d.subscribe("*");
    }

    public void CloseConnection(){
        try {
            nc.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    private static GenericData.Record AvroDeserialize(byte [] bytes, Schema schema){
        Injection<GenericData.Record, byte[]> recordInjection;
        recordInjection = GenericAvroCodecs.toBinary(schema);
        return recordInjection.invert(bytes).get();
    }
}
