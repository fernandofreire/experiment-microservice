import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

class RabbitmqSender implements Sender {
    private final String exchange_name;
    private Connection connection;
    private Channel channel;

    RabbitmqSender() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        exchange_name = "EXCHANGE";
        System.out.println("create sender ");
        factory.setUsername("cep-handler");
        factory.setPassword("g7wavG!WZ&5cYXu@yeR6nnho97A7Sd");
        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(exchange_name, "topic");
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void CloseConnection() {
        try {
            channel.close();
            connection.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void send(String routingKey, byte [] message)  {
        try {
            //System.out.print(".");
            channel.basicPublish(exchange_name, routingKey, null, message);
            //channel.basicPublish(exchange_name, "basic."+routingKey, null, message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}