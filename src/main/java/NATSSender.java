import io.nats.client.Connection;
import io.nats.client.Nats;

import java.io.IOException;

public class NATSSender implements Sender {

    private Connection nc;

    public NATSSender() {
        String Natshost = System.getenv("NATS_HOST");
        int NATSPort = Integer.parseInt(System.getenv("NATS_PORT"));

        try {
            nc = Nats.connect("nats://"+Natshost+":"+NATSPort);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void CloseConnection(){
        try {
            nc.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void send(String subject, byte [] message){
        nc.publish(subject,message);
    }
}
