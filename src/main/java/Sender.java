public interface Sender {

    void CloseConnection();

    void send(String routingKey, byte [] message);

}
